<?php

return array(

    'VaetasNewsiOS'     => array(
        'environment' =>'development',
        'certificate' => base_path().'/certificates/vaetasnewsPushDevelopment.pem',
        'passPhrase'  => env('IOS_CERTIFICATE_PASSPHRASE'),
        'service'     =>'apns'
    ),
    'VaetasNewsAndroid' => array(
        'environment' =>'production',
        'apiKey'      =>env('ANDROID_API_KEY'),
        'service'     =>'gcm'
    )

);