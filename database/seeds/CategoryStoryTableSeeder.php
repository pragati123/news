<?php

use App\Category;
use App\Story;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryStoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('category_story')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        $categories = Category::all()->pluck('id')->toArray();
        $stories = Story::all()->pluck('id')->toArray();

        $faker = Factory::create();

        foreach ($stories as $story) {
            DB::table('category_story')->insert([
                'category_id' => $faker->randomElement($categories),
                'story_id' => $story,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        Category::reguard();
    }
}
