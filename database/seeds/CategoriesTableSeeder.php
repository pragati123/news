<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('categories')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');


        Category::create([
            'title' => 'SEO'
        ]);

        Category::create([
            'title' => 'Social Media'
        ]);

        Category::create([
            'title' => 'Content Marketing'
        ]);

        Category::create([
            'title' => 'Sales'
        ]);

        Category::create([
            'title' => 'Advertising'
        ]);

        Category::create([
            'title' => 'Video Sales'
        ]);

        Category::reguard();
    }
}
