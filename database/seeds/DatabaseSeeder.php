<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(StoriesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(CategoryStoryTableSeeder::class);
        $this->call(DeviceTokenSeeder::class);
    }
}
