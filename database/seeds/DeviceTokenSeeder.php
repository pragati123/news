<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DeviceTokenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('device_tokens')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');


        DB::table('device_tokens')->insert([
            'token' => '740f4707bebcf74f9b7c25d48e3358945f6aa01da5ddb387462c7eaf61bb78ad',
            'platform' => 'ios',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('device_tokens')->insert([
            'token' => 'b47cd101df52a281',
            'platform' => 'android',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
