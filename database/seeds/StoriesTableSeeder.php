<?php

use App\Jobs\UrlShortenerJob;
use App\Story;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\DB;

class StoriesTableSeeder extends Seeder
{
    use DispatchesJobs;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Story::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('stories')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        $newStory = Story::create([
            'title' => 'Oxford dictionary adds \'ROFL\' and \'tl;dr\'',
            'description' => 'The Oxford English Dictionary has added the words ROFL (Rolling on Floor Laughing) and tl;dr (too long; didn’t read) in its June 2016 update. The update for this quarter also includes the words glamping (glamourous camping), budgie smugglers (close-fitting men’s swimming trunks) and listicle (an online article in the form of a list).',
            'article_link' => 'https://www.inshorts.com/news/oxford-dictionary-adds-rofl-and-tldr-1467738170323',
            'thumbnail_url' => '7a0beb97-6ea8-4f6d-b7a8-ab3cccdfd1cb-1-1467738170327.jpg'
        ]);

        $job = new UrlShortenerJob($newStory);
        $this->dispatch($job);

        $newStory = Story::create([
            'title' => 'Twitter\'s VP of International Operations quits',
            'description' => 'Micro-blogging site Twitter\'s Vice President of International Operations, Shailesh Rao has quit the company. "After 4+ yrs growing the Intl business at @twitter, it’s time for new challenges. Around through July, then off to plot the next chapter!," tweeted Rao. Before joining Twitter in 2012, Rao worked with Google for seven years.',
            'article_link' => 'http://techcircle.vccircle.com/2016/07/05/twitter-asia-chief-shailesh-rao-resigns',
            'thumbnail_url' => 'd9de9a80-7842-42ff-ad5e-b401a65a8a97-1-1467737733732.jpg',
            'published_at' => '2016-06-12 15:37:32'
        ]);

        $job = new UrlShortenerJob($newStory);
        $this->dispatch($job);

        $newStory = Story::create([
            'title' => 'BlackBerry announces to kill its Classic model',
            'description' => 'Canadian smartphone maker BlackBerry today announced that it will no longer manufacture its Classic model, one of the company’s last smartphone models with a physical keyboard to input text. "The Classic has long surpassed the average lifespan for a smartphone in today’s market," said BlackBerry. The company is reportedly launching three new Android-based smartphones in the next three quarters.',
            'article_link' => 'https://techcrunch.com/2016/07/05/blackberry-confirms-it-will-can-its-classic-smartphone',
            'thumbnail_url' => '7fd75300-d886-41a7-b468-e286db962f61-1-1467737720160.jpg'
        ]);

        $job = new UrlShortenerJob($newStory);
        $this->dispatch($job);

        $newStory = Story::create([
            'title' => 'Turkey arrests 17 over Istanbul attack',
            'description' => 'Turkey has arrested 17 people, mostly foreigners, suspected to be responsible for last week\'s triple suicide bombing of Istanbul\'s Ataturk Airport, which killed 45 people and injured hundreds. Turkish President Tayyip Erdogan claims the attack to be the work of Islamic State (IS) militants from the ex-Soviet Union. A total of 30 people are now pending trial for the case.',
            'article_link' => 'http://www.reuters.com/article/us-turkey-blasts-arrests-idUSKCN0ZL0F4?',
            'thumbnail_url' => 'ba8a9b56-aa3f-4de9-b9f3-4ac560db7899-1-1467737330850.jpg',
            'published_at' => '2016-07-12 22:54:35'
        ]);

        $job = new UrlShortenerJob($newStory);
        $this->dispatch($job);

        $newStory = Story::create([
            'title' => 'Rare Chinese vase sells for $847,499 in England',
            'description' => 'A rare Chinese vase which was reportedly being used as a doorstop for 36 years has sold for £650,000 (about $847,499) at an auction in Derbyshire, England. Hansons Auctioneers said the seller inherited it from a relative who acquired it during the 1920s in Cornwall, England. It is believed to date back to the 18th century during Emperor Qianlong\'s reign.',
            'article_link' => 'https://www.inshorts.com/news/rare-chinese-vase-sells-for-%24847499-in-england-1467736821907',
            'thumbnail_url' => '56c7bd64-75c2-45ab-bc98-7004bb6c2f75-1-1467736821911.jpg',
            'published_at' => '2016-06-21 22:54:35'
        ]);

        $job = new UrlShortenerJob($newStory);
        $this->dispatch($job);

        $newStory = Story::create([
            'title' => 'WikiLeaks publishes 1000 Clinton\'s War emails',
            'description' => 'WikiLeaks has launched an archive for over 30,000 emails and attachments linked to Hillary Clinton\'s private email server, over 1000 of which are pertaining to the Iraq war. The State Department made the data available as a result of the Freedom of Information Act. Notably, in 2015 it was revealed that Hillary used her family\'s email server for official communications.',
            'article_link' => 'https://wikileaks.org/clinton-emails/?q=iraq%7Cbaghdad%7Cbasra%7Cmosoul&mfrom=&mto=&title=&notitle=&date_from=&date_to=&nofrom=&noto=&count=50&sort=0',
            'thumbnail_url' => 'f13715a1-8747-435d-820f-7e4c20229928-1-1467736781774.jpg',
            'published_at' => '2016-05-08 22:54:35'
        ]);

        $job = new UrlShortenerJob($newStory);
        $this->dispatch($job);

        $newStory = Story::create([
            'title' => 'Serena reaches her 10th Wimbledon semifinal',
            'description' => 'Wimbledon top-seed Serena Williams beat Russia’s Anastasia Pavlyuchenkova 6-4, 6-4, on Tuesday to progress to the Wimbledon semifinal. Serena has won the Wimbledon six times and this will be her 10th Wimbledon semifinal. She is set to face Russia’s Elena Vesnina in the semi-final on Thursday.',
            'article_link' => 'http://www.sportskeeda.com/tennis/wimbledon-2016-defending-champion-serena-williams-makes-semi-finals',
            'thumbnail_url' => 'fe33f81e-1fb4-4042-92b1-19591bee0297-1-1467736574932.jpg',
            'published_at' => '2016-06-02 22:54:35'
        ]);

        $job = new UrlShortenerJob($newStory);
        $this->dispatch($job);

        $newStory = Story::create([
            'title' => 'Designer makes flash photography-proof scarf',
            'description' => 'Dutch-born fashion entrepreneur Saif Siddiqui has designed a scarf which protects its wearer from flash photography by reflecting light back into the camera. The \'ISHU\' scarf has been sported by celebrities including Bayern Munich footballer Jerome Boateng, socialite Paris Hilton and actress Cameron Diaz. The scarf can be pre-ordered online for about $377.',
            'article_link' => 'http://www.telegraph.co.uk/news/2016/06/30/this-genius-paparazzi-proof-scarf-can-make-you-invisible-in-phot/',
            'thumbnail_url' => 'bed0c668-836b-4757-b502-cd773d24a62a-1-1467736558660.jpg',
            'published_at' => '2016-06-30 22:54:35'
        ]);

        $job = new UrlShortenerJob($newStory);
        $this->dispatch($job);

        $newStory = Story::create([
            'title' => 'title',
            'description' => 'description',
            'article_link' => 'http://link-to-something.com/sub',
            'thumbnail_url' => 'bed0c668-836b-4757-b502-cd773d24a62a-1-1467736558660.jpg',
            'published_at' => '2016-06-26 22:54:35'
        ]);

        $job = new UrlShortenerJob($newStory);
        $this->dispatch($job);

        $newStory = Story::create([
            'title' => 'another title',
            'description' => 'another description',
            'article_link' => 'http://link-to-something.com/sub',
            'thumbnail_url' => 'bed0c668-836b-4757-b502-cd773d24a62a-1-1467736558660.jpg',
            'published_at' => '2016-07-07 22:54:35'
        ]);

        $job = new UrlShortenerJob($newStory);
        $this->dispatch($job);

        $newStory = Story::create([
            'title' => 'yet another title',
            'description' => 'yet another description',
            'article_link' => 'http://link-to-something.com/sub',
            'thumbnail_url' => 'bed0c668-836b-4757-b502-cd773d24a62a-1-1467736558660.jpg',
            'published_at' => '2016-07-03 22:54:35'
        ]);

        $job = new UrlShortenerJob($newStory);
        $this->dispatch($job);

        $newStory = Story::create([
            'title' => 'final title',
            'description' => 'final description',
            'article_link' => 'http://link-to-something.com/sub',
            'thumbnail_url' => 'bed0c668-836b-4757-b502-cd773d24a62a-1-1467736558660.jpg',
            'published_at' => '2016-03-12 22:54:35'
        ]);

        $job = new UrlShortenerJob($newStory);
        $this->dispatch($job);
        $stories = Story::all();
        foreach ($stories as $story)
        {
            $story->slug = str_slug($story->title);
            $story->save();
        }

        Story::reguard();
    }

}
