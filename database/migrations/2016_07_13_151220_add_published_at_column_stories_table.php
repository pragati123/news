<?php

use App\Story;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AddPublishedAtColumnStoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stories', function (Blueprint $table) {
            $table->dateTime('published_at')->nullable();
        });

        $stories = Story::all();
        foreach ($stories as $story) {
            if ($story->published == 1) {
                $story->published_at = $story->created_at;
                $story->save();
            }
        }

        Schema::table('stories', function ($table) {
            $table->dropColumn('published');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stories', function (Blueprint $table) {
            $table->boolean('published');
        });

        $stories = Story::all();
        foreach ($stories as $story) {
            if ($story->published_at == null) {
                $story->published = false;
                $story->save();
            } else {
                $story->published = true;
                $story->save();
            }
        }

        Schema::table('stories', function ($table) {
            $table->dropColumn('published_at');
        });
    }
}
