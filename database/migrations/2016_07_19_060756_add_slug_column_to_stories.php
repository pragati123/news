<?php

use App\Story;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AddSlugColumnToStories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stories', function (Blueprint $table) {
            $table->string('slug')->nullable();
        });

        $stories = Story::all();
        foreach ($stories as $story)
        {
            $story->slug = str_slug($story->title);
            $story->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stories', function ($table) {
            $table->dropColumn('slug');
        });
    }
}
