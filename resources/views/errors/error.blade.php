@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="content">
            <div class="error_title">{{ isset($error) ? $error : 'Some Error Occurred' }}</div>
        </div>
    </div>
@endsection

@section('style')
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            color: #B0BEC5;
        }

        .container {
            text-align: center;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .error_title {
            width: 100%;
            font-weight: 100;
            font-size: 48px;
            margin-bottom: 40px;
        }
    </style>
@endsection
