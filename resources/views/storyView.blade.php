@extends('layouts.app')
@section('meta')
    {{--Twitter--}}
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="{{$story->title}}">
    <meta name="twitter:description" content="{{$story->description}}">
    <meta name="twitter:image" content="{{("$story->thumbnail_url")}}">

    {{--Facebook--}}
    <meta property="fb:app_id" content="1648507378769295" >
    <meta property="og:url" content="{{url("/stories/$story->slug")}}" >
    <meta property="og:type" content="Startup Briefs">
    <meta property="og:title" content="{{$story->title}}" >
    <meta property="og:description" content="{{$story->description}}" >
    <meta property="og:image" content="{{("$story->thumbnail_url")}}" >
    <meta property="og:image:width" content="100" >
    <meta property="og:image:height" content="100" >
    <meta property="og:rich_attachment" content="true">

    {{--Google--}}
    <meta name="author" content="Invidz, LLC" />
    <meta name="copyright" content="Invidz, LLC" />
    <meta name="application-name" content="VaetasNews" />
@endsection
<title>
    {{$story->title}}
</title>
@section('header')
    <link rel="stylesheet" href={{ url('/css/stories.css') }}>
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="ck-card ck-center">
                <div class="ck-news-image">
                    <img src="{{ $story->thumbnail_url }}" class="ck-thumbnail">
                </div>
                <div class="ck-pull-right ck-card-title">
                    <a target="_blank" style="color:#44444d!important" href="{{ $story->article_link }}">
                        {{ $story->title }}
                        @if (!Auth::guest())
                            <a class="pull-right" href="{{ url("/stories/$story->id/edit") }}"><i class="fa fa-pencil"></i></a>
                        @endif
                    </a>
                </div>
                <div class="ck-pull-right ck-card-content">
                    {{ $story->description }}
                </div>
            </div>
        </div>
    </div>
@endsection
