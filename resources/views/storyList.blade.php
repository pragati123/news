@extends('layouts.app')
<title>Stories List</title>

@section('header')
    <link rel="stylesheet" href={{ url('/css/stories.css') }}>
@endsection

@section('meta')
    {{--Twitter--}}
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Startup Briefs">
    <meta name="twitter:description"
          content="Startup Briefs is a platform which reduces the length of news while retaining the content, hence saving time of reader">
    <meta name="twitter:image" content="https://s3.amazonaws.com/vaetas/images/startup_briefs_logo.png">

    {{--Facebook--}}
    <meta property="fb:app_id" content="1648507378769295">
    <meta property="og:url" content="{{ url('/') }}">
    <meta property="og:type" content="Startup Briefs">
    <meta property="og:title" content="Startup Briefs">
    <meta property="og:description"
          content="Startup Briefs is a platform which reduces the length of news while retaining the content, hence saving time of reader">
    <meta property="og:image" content="https://s3.amazonaws.com/vaetas/images/startup_briefs_logo.png">
    <meta property="og:rich_attachment" content="true">

    {{--Google--}}
    <meta name="author" content="Invidz, LLC"/>
    <meta name="copyright" content="Invidz, LLC"/>
    <meta name="application-name" content="VaetasNews"/>
@endsection

@section('content')
    <div class="container">
        @if(isset($edited))
            <div id="alert" class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <strong>Success!</strong> Your Story was edited successfully!
            </div>
        @endif
        @if(isset($deleted))
            <div id="alert" class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <strong>Success!</strong> Your Story was deleted successfully!
            </div>
        @endif
        @if(isset($forEdit))
            <div class="row ck-center" style="width: 900px;">
                <form action="{{url('/fetchLink')}}" method="post">
                    <div class="form-inline">
                        {{csrf_field()}}
                        <input type="text" style="width: 800px;" id="link" name="link" class="form-control"
                               placeholder="Enter the link of an external news.">
                        <input type="submit" class="btn btn-primary pull-right" value="Submit">
                    </div>
                </form>
            </div>

        @endif
        <div class="row">
            @foreach($stories as $story)
                <div class="ck-card ck-center">
                    <div class="ck-news-image">
                        <img src="{{ $story->thumbnail_url }}" class="ck-thumbnail">
                    </div>
                    @if (!Auth::guest())
                        <a class="pull-right" href="{{ url("/stories/$story->id/edit") }}"><i
                                    class="fa fa-lg fa-pencil"></i></a>
                    @endif
                    <div class="ck-pull-right ck-card-title">
                        <a target="_blank" style="color:#44444d!important"
                           href="{{ $story->article_link }}">
                            {{ $story->title }}
                        </a>
                    </div>
                    <div class="ck-pull-right ck-card-content">
                        {{ $story->description }}
                    </div>

                    <div class="ck-share">
                        <a target="_blank"
                           href="https://www.facebook.com/sharer.php?u={{urlencode(url("/stories/$story->slug"))}}"><i
                                    class="fa fa-lg fa-facebook-official" aria-hidden="true"></i></a>
                        <a target="_blank"
                           href="https://plus.google.com/share?url={{url("/stories/$story->slug")}}"><i
                                    class="fa fa-lg fa-google-plus" aria-hidden="true"></i></a>
                        <a target="_blank"
                           href="https://www.twitter.com/share?text={{$story->title}}&orginal_referer={{url("/stories/$story->slug")}}&url={{url("/stories/$story->slug")}}"><i
                                    class="fa fa-lg fa-twitter" aria-hidden="true"></i></a>
                        <a target="_blank"
                           href="https://www.linkedin.com/shareArticle?mini=true&url={{urlencode(url("/stories/$story->slug"))}}&title={{urlencode($story->title)}}&summary={{urlencode($story->description)}}&source=Startup Briefs"><i
                                    class="fa fa-lg fa-linkedin" aria-hidden="true"></i></a>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row">
            <div class="ck-pagination">
                {{ $stories->render() }}
            </div>
        </div>
    </div>
    <script>
        jQuery(document).ready(function ($) {
            $('#alert').delay(5000).slideUp('slow');
        });
    </script>
@endsection