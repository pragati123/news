@extends('layouts.app')
        <title>Create Story</title>

        <!-- Main Content -->
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create a new Story</div>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('/stories/create') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="title" class="col-md-4 control-label">Title</label>

                                <div class="col-md-6">
                                    <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}">

                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="description" class="col-md-4 control-label">Description</label>

                                <div class="col-md-6">
                                    <textarea id="description" class="form-control" name="description"
                                              style="max-width: 100%">{{ isset($attrs) ?$attrs["description"] :old('description') }}</textarea>

                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('article_link') ? ' has-error' : '' }}">
                                <label for="article_link" class="col-md-4 control-label">Article Link</label>

                                <div class="col-md-6">
                                    <input id="article_link" type="text" class="form-control" name="article_link"
                                           value="{{ isset($attrs) ?$attrs["article_link"] :old('article_link') }}">

                                    @if ($errors->has('article_link'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('article_link') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('thumbnail') ? ' has-error' : '' }}">
                                <label for="thumbnail" class="col-md-4 control-label">Thumbnail</label>

                                <div class="col-md-6">
                                    <input id="thumbnail" type="file" class="form-control" name="thumbnail">

                                    @if ($errors->has('thumbnail'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('thumbnail') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('categories') ? ' has-error' : '' }}">
                                <label for="duration" class="col-md-4 control-label">Category</label>
                                <div class="col-md-6" id="multi_select">
                                    <select id="select" name="categories[]" class="js-example-basic-multiple js-states form-control" multiple="multiple">
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->title }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('categories'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('categories') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="published" value=1 {{old('published') != null ?'checked' :'' }}>Publish
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Create This Story
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.js-example-basic-multiple').select2({
                placeholder: "Select a category",
                allowClear: true
            });
        });
    </script>
@endsection
