<?php
/**
 * Created by PhpStorm.
 * User: piyushkantm
 * Date: 25/06/16
 * Time: 13:01
 */

namespace App\Services;


use App\Exceptions\ThumbnailNotFoundException;
use Illuminate\Support\Facades\Storage;

class ThumbnailService
{
    public function show($thumbnail) {
        if (Storage::disk('local')->exists('/thumbnails/' . $thumbnail)) {
            $file = Storage::disk('local')->get('/thumbnails/' . $thumbnail);
            return $file;
        } else {
            throw new ThumbnailNotFoundException();
        }
    }
}