<?php
/**
 * Created by PhpStorm.
 * User: piyushkantm
 * Date: 12/07/16
 * Time: 05:07
 */

namespace App\Services\Contracts;


interface DeviceTokenCreateContract
{
    public function getDeviceToken();
}