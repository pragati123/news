<?php
/**
 * Created by PhpStorm.
 * User: kushagra
 * Date: 06/07/16
 * Time: 10:20 PM
 */

namespace App\Services\Contracts;


interface StoryEditContract
{
    public function hasPublishedAt();
    public function hasThumbnail();
    public function getTitle();
    public function getDescription();
    public function getArticleLink();
    public function getThumbnail();
    public function getCategories();
    public function getPublishedAt();
}