<?php
/**
 * Created by PhpStorm.
 * User: piyushkantm
 * Date: 26/06/16
 * Time: 01:18
 */

namespace App\Services\Contracts;


interface StoryListContract
{
    public function hasCategories();
    public function getCategories();
}