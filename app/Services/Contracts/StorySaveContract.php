<?php
/**
 * Created by PhpStorm.
 * User: piyushkantm
 * Date: 13/06/16
 * Time: 23:16
 */

namespace App\Services\Contracts;


interface StorySaveContract
{
    function hasTitle();
    function hasDescription();
    function hasArticleLink();
    function hasPublishedAt();
    function getTitle();
    function getDescription();
    function getThumbnail();
    function getArticleLink();
    function getPublishedAt();
    function getCategories();
}