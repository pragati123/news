<?php
/**
 * Created by PhpStorm.
 * User: piyushkantm
 * Date: 26/06/16
 * Time: 17:45
 */

namespace App\Services;


use App\Category;

class CategoryService
{
    public function index() {
        $categories = Category::orderBy('title', 'asc')->get();
        return $categories;
    }

}