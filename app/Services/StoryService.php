<?php
/**
 * Created by PhpStorm.
 * User: piyushkantm
 * Date: 13/06/16
 * Time: 23:15
 */

namespace App\Services;


use App\Jobs\UrlShortenerJob;
use App\Services\Contracts\StoryEditContract;
use App\Services\Contracts\StoryListContract;
use App\Services\Contracts\StorySaveContract;
use App\Story;
use Davibennun\LaravelPushNotification\Facades\PushNotification;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Sly\NotificationPusher\Adapter;
use Sly\NotificationPusher\Adapter\Apns;
use Sly\NotificationPusher\Adapter\Gcm;

class StoryService
{
    use DispatchesJobs;

    private $disk;

    function __construct()
    {
        // Filesystem Configuration
        $this->disk = Storage::disk('s3');
    }

    /**
     * @param StoryListContract $request
     * @return mixed
     */
    function index(StoryListContract $request)
    {
        if ($request->hasCategories()) {
            $stories = $this->whereInCategories($request->getCategories())
                ->whereNotNull('published_at')
                ->orderBy('published_at', 'dsc')
                ->paginate(10);
        } else {
            return $this->published();
        }
        return $stories;
    }

    /**
     * @return mixed
     */
    function published()
    {
        $stories = Story::whereNotNull('published_at')->orderBy('published_at', 'dsc')->paginate(10);
        return $stories;
    }

    /**
     * @return mixed
     */
    function unpublished()
    {
        $stories = Story::whereNull('published_at')->orderBy('created_at', 'desc')->paginate(10);
        return $stories;
    }

    /**
     * @param $categories
     * @return Query Builder
     */
    public function whereInCategories($categories)
    {
        return Story::whereHas('categories', function ($q) use ($categories) {
            $q->whereIn('categories.id', $categories);
        });
    }

    /**
     * @param StorySaveContract $request
     * @return Story
     */
    function store(StorySaveContract $request = null)
    {
        $story = new Story();

        $story->title = $request->getTitle();
        $story->description = $request->getDescription();
        $story->article_link = $request->getArticleLink();
        $story->slug = str_slug($story->title);

        if ($request->hasPublishedAt()) {
            $story->published_at = $request->getPublishedAt();
        }

        $thumbnail = $request->getThumbnail();
        $id = uniqid();
        $name = $id . '-' . $thumbnail->getClientOriginalExtension();
        $story->thumbnail_url = $name;
        $this->disk->put('/images/' . $name, file_get_contents($thumbnail->getRealPath()), 'public');

        $story->save();

        $story->categories()->attach($request->getCategories());

        $job = new UrlShortenerJob($story);
        $this->dispatch($job);

        if ($request->hasPublishedAt()) {
            $this->pushNotificationsiOS($story);
            $this->pushNotificationsAndroid($story);
        }

        return $story;
    }

    /**
     * @param Story $story
     * @param StoryEditContract $request
     * @return Story
     */
    public function update(Story $story, StoryEditContract $request)
    {
        $story->title = $request->getTitle();
        $story->description = $request->getDescription();

        if ($story->published_at == null && $request->hasPublishedAt()) {
            $story->published_at = $request->getPublishedAt();
        } else if (!$request->hasPublishedAt()) {
            $story->published_at = null;
        }

        if ($request->hasThumbnail()) {
            //Deleting old thumbnail
            $this->disk->delete($story->getThumbnailUrlAttribute($story->thumbnail_url));
            $thumbnail = $request->getThumbnail();
            $name = uniqid() . '-' . $thumbnail->getClientOriginalName();
            $story->thumbnail_url = $name;

            $this->disk->put('/images/' . $name, file_get_contents($thumbnail->getRealPath()), 'public');
        }
        $categories = $story->categories;
        foreach ($categories as $c) {
            $story->categories()->detach($c->id);
        }
        $story->categories()->attach($request->getCategories());


        if ($story->article_link != $request->getArticleLink()) {
            $story->article_link = $request->getArticleLink();
            $job = new UrlShortenerJob($story);
            $this->dispatch($job);
        }

        $story->save();
        return $story;
    }

    private function pushNotificationsiOS(Story $story)
    {
        $deviceTokens = DB::table('device_tokens')->where('platform', 'ios')->pluck('token');
        $apns = new Apns([
            'certificate' => base_path('certificates/vaetasnewsPushDevelopment.pem')
        ]);

        $devices = array();

        foreach ($deviceTokens as $deviceToken) {
            if ($apns->supports($deviceToken)) {
                array_push($devices, PushNotification::Device($deviceToken));
            } else {
                Log::info("rejected $deviceToken");
            }
        }
        $deviceCollection = PushNotification::DeviceCollection($devices);

        try {
            $collection = PushNotification::app(Config::get('services.apps.ios'))
                ->to($deviceCollection)
                ->send($story->title);

            foreach ($collection->pushManager as $push) {
                $response = $push->getAdapter()->getResponse();
            }
        } catch (\Exception $e) {
            Log::error("couldn't send push notification $e->getMessage()");
        }
    }

    private function pushNotificationsAndroid(Story $story)
    {
        $deviceTokens = DB::table('device_tokens')->where('platform', 'android')->pluck('token');
//        $gcm = new Gcm;

        $devices = array();

        foreach ($deviceTokens as $deviceToken) {
            if (preg_match('/[0-9a-zA-Z\-\_]/i', $deviceToken) === true) {
                array_push($devices, PushNotification::Device($deviceToken));
            } else {
                Log::info("rejected $deviceToken");
            }
        }
        $deviceCollection = PushNotification::DeviceCollection($devices);

        try {
            $collection = PushNotification::app(Config::get('services.apps.android'))
                ->to($deviceCollection)
                ->send($story->title);

            foreach ($collection->pushManager as $push) {
                $response = $push->getAdapter()->getResponse();
            }
        } catch (\Exception $e) {
            Log::error("couldn't send push notification $e->getMessage()");
        }
    }

    function createStoryFormExternalLink($title, $description, $imageUrl, $articleLink)
    {
        $story = new Story();

        $story->title = $title;
        $story->description = $description;
        $story->article_link = $articleLink;
        $story->slug = str_slug($story->title);
        $story->published_at = null;

        $thumbnail = file_get_contents($imageUrl);
        $id = uniqid();
        $name = 'images/' . $id;
        $story->thumbnail_url = $id;

        $this->disk->put($name, $thumbnail, 'public');

        $story->save();

        $job = new UrlShortenerJob($story);
        $this->dispatch($job);

        return $story;
    }

    public function delete($id)
    {
        $story = Story::where('id', $id)->get();
        $story[0]->delete();
    }
}