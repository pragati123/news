<?php
/**
 * Created by PhpStorm.
 * User: piyushkantm
 * Date: 19/06/16
 * Time: 11:15
 */

namespace App\Services;


use Curl\Curl;
use Illuminate\Support\Facades\Config;

class CKGoogleService
{
    protected $curl;

    /**
     * APIService constructor.
     */
    public function __construct()
    {
        $this->curl = new Curl();
    }

    public function generateShortLink($url) {
        $data = [
            'longUrl' => $url
        ];

        $dataJSON = json_encode($data);

        $url = Config::get('services.google.link_shortener_url');
        $url .= '?key=' . Config::get('services.google.key');

        $this->curl->setHeader('Content-Type', 'application/json');
        $this->curl->setHeader('Content-Length', strlen($dataJSON));
        $this->curl->post($url,$dataJSON);
        if ($this->curl->error) {
            $response = $this->curl->rawResponse;
            return $response;
        } else {
            $response = $this->curl->response->id;
            return $response;
        }

    }
}