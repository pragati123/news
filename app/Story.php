<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Config;

class Story extends Model
{
    use SoftDeletes;

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = [
        'deleted_at'
    ];

    protected $table = 'stories';

    public function getThumbnailUrlAttribute($value) {
        $url = 'https://s3.amazonaws.com/' . Config::get('filesystems.disks.s3.bucket') . '/images/' . $value;
        return url($url);
    }

    public function categories() {
        return $this->belongsToMany(Category::class)->withTimestamps();
    }
}
