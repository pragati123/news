<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::auth();

    Route::group(['middleware' => ['auth']], function () {
        Route::get('/stories/create', 'HomeController@index');
        Route::post('/stories/create', 'HomeController@store');

        Route::get('/stories/unpublished', 'StoryController@unpublishedStories');
        Route::get('/stories/{id}/edit', 'StoryController@show');
        Route::put('/stories/{id}/edit', 'StoryController@edit');
        Route::delete('/stories/{id}', 'StoryController@delete');
    });

    Route::get('/stories', 'StoryController@index');
    Route::post('/fetchLink', 'StoryController@fetchExternalLink');
    Route::get('/', 'StoryController@listStories');
    Route::get('/login', 'HomeController@showLoginPage');
    Route::get('/stories/{slug}', 'StoryController@storyView');
});


Route::group(['middleware' => 'api'], function () {
    Route::get('/categories', 'CategoryController@index');
    Route::get('/thumbnails/{thumbnail}', 'ThumbnailController@show');
});