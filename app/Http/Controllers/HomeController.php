<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\StoryCreateRequest;
use App\Services\CategoryService;
use App\Services\StoryService;

class HomeController extends Controller
{
    protected $storyService;
    protected $categoryService;
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->storyService = new StoryService();
        $this->categoryService = new CategoryService();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->categoryService->index();
        return view('form')->with('categories', $categories);
    }

    /**
     * @param StoryCreateRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(StoryCreateRequest $request) {
        $this->storyService->store($request);

        return view('success');
    }

    public function showLoginPage()
    {
        return view('auth.login');
    }
}
