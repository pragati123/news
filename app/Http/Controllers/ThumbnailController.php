<?php

namespace App\Http\Controllers;

use App\Services\ThumbnailService;
use Illuminate\Http\Request;

use App\Http\Requests;

class ThumbnailController extends Controller
{
    protected $thumbnailService;

    public function __construct()
    {
        $this->thumbnailService = new ThumbnailService();
    }

    public function show($thumbnail) {
        $file = $this->thumbnailService->show($thumbnail);
        return response($file)->header('Content-Type', 'image/jpeg');
    }
}
