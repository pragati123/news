<?php

namespace App\Http\Controllers;

use App\Category;
use App\Services\CategoryService;
use Illuminate\Http\Request;

use App\Http\Requests;

class CategoryController extends Controller
{
    protected $categoryService;

    public function __construct()
    {
        $this->categoryService = new CategoryService();
    }

    public function index() {
        return $this->categoryService->index();
    }
}
