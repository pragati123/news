<?php
/**
 * Created by PhpStorm.
 * User: kushagra
 * Date: 13/06/16
 * Time: 11:54 PM
 */

namespace App\Http\Controllers;


use App\Http\Requests\FetchExternalStoryRequest;
use App\Http\Requests\Request;
use App\Http\Requests\StoryEditRequest;
use App\Http\Requests\StoryListRequest;
use App\Services\CategoryService;
use App\Services\StoryService;
use App\Story;
use Curl\Curl;
use Illuminate\Support\Facades\DB;
use Symfony\Component\DomCrawler\Crawler;

class StoryController extends Controller
{
    protected $storyService;
    protected $categoryService;

    public function __construct()
    {
        $this->storyService = new StoryService();
        $this->categoryService = new CategoryService();
    }

    public function index(StoryListRequest $request)
    {
        return $this->storyService->index($request);
    }

    public function listStories(StoryListRequest $request)
    {
        $stories = $this->storyService->published();
        return view('storyList')
            ->with('stories', $stories)
            ->with('edited', $request->session()->get('edited'))
            ->with('deleted', $request->session()->get('deleted'));
    }

    public function unpublishedStories()
    {
        $stories = $this->storyService->unpublished();
        return view('storyList')
            ->with('stories', $stories)
            ->with('forEdit', true);
    }

    public function show($id)
    {
        $categories = $this->categoryService->index();
        $story = Story::where('id', $id)->get();
        $selectedCategories = $story[0]->categories;

        return view('editForm')
            ->with('story', $story[0])
            ->with('selectedCategories', $selectedCategories)
            ->with('categories', $categories->diff($selectedCategories));
    }

    public function edit(StoryEditRequest $request, $id)
    {
        $story = Story::where('id', $id)->get();
        $this->storyService->update($story[0], $request);
        return redirect('/')->with('edited', 'true');
    }

    public function delete($id)
    {
        $this->storyService->delete($id);
        return redirect('/')->with('deleted', 'true');
    }

    public function storyView($slug)
    {
        $story = Story::where('slug', $slug)->get();
        dd($story->getOriginal('thumbnail_url'));
        return view('storyView')
            ->with('story', $story[0]);
    }

    function fetchExternalLink(FetchExternalStoryRequest $request)
    {
        $curl = new Curl();
        $r = $curl->get($request->get('link'));
        if ($curl->error) {
            return view('errors.error')
                ->with('error', $curl->errorMessage);
        }
        $crawler = new Crawler($r);
        $attrs = [];
        $attrs["title"] = $crawler->filter("head > title")->text();
        $attrs["description"] = $crawler->filter('meta[name=description]')->attr('content');
        $crawler->filter('meta')->each(function (Crawler $node) use (&$attrs) {
            if ($node->attr('name') == 'og:image') {
                $attrs["image_url"] = $node->attr('content');
            }

            if (is_null($attrs["description"])) {
                if ($node->attr('name') == 'og:description') {
                    $attrs["description"] = $node->attr('content');
                }
            }

            if ($node->attr('property') == 'og:image') {
                $attrs["image_url"] = $node->attr('content');
            }
        });
        $attrs["article_link"] = $request->get('link');
        $r = $this->storyService->createStoryFormExternalLink($attrs["title"], $attrs["description"], $attrs["image_url"], $attrs["article_link"]);
        return redirect('/stories/unpublished');
    }
}