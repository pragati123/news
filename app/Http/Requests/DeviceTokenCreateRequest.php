<?php
/**
 * Created by PhpStorm.
 * User: piyushkantm
 * Date: 12/07/16
 * Time: 05:07
 */

namespace App\Http\Requests;



use App\Services\Contracts\DeviceTokenCreateContract;

class DeviceTokenCreateRequest extends Request implements DeviceTokenCreateContract
{
    const TOKEN = 'token';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            self::TOKEN => 'required|string',
        ];
    }


    public function getDeviceToken()
    {
        return $this->get(self::TOKEN);
    }
}