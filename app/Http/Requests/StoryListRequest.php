<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Services\Contracts\StoryListContract;

class StoryListRequest extends Request implements StoryListContract
{
    const CATEGORY = 'category';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            self::CATEGORY => 'array',
            self::CATEGORY . '.*' => 'exists:categories,id'
        ];
    }

    public function hasCategories()
    {
        return $this->has(self::CATEGORY);
    }

    public function getCategories()
    {
        return $this->get(self::CATEGORY);
    }
}
