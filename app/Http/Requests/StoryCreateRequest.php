<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Services\Contracts\StorySaveContract;
use Carbon\Carbon;

class StoryCreateRequest extends Request implements StorySaveContract
{
    const TITLE = 'title';
    const DESCRIPTION = 'description';
    const ARTICLE_LINK = 'article_link';
    const THUMBNAIL = 'thumbnail';
    const PUBLISHED = 'published';
    const CATEGORY = 'categories';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            self::TITLE => 'required|string',
            self::DESCRIPTION => 'required|string',
            self::ARTICLE_LINK => 'required|active_url',
            self::THUMBNAIL => 'image|required',
            self::PUBLISHED => 'boolean',
            self::CATEGORY => 'array|required',
            self::CATEGORY . '.*' => 'exists:categories,id'
        ];
    }

    function hasTitle()
    {
        return $this->has(self::TITLE);
    }

    function hasDescription()
    {
        return $this->has(self::DESCRIPTION);
    }

    function hasArticleLink()
    {
        return $this->has(self::ARTICLE_LINK);
    }

    function hasPublishedAt()
    {
        return $this->has(self::PUBLISHED);
    }

    function getTitle()
    {
        return $this->get(self::TITLE);
    }

    function getDescription()
    {
        return $this->get(self::DESCRIPTION);
    }

    function getArticleLink()
    {
        return $this->get(self::ARTICLE_LINK);
    }

    function getThumbnail()
    {
        return $this->file(self::THUMBNAIL);
    }

    function getPublishedAt()
    {
        return Carbon::now();
    }

    public function getCategories()
    {
        return $this->get(self::CATEGORY);
    }
}
