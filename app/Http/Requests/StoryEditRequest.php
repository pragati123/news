<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Services\Contracts\StoryEditContract;
use Carbon\Carbon;

class StoryEditRequest extends Request implements StoryEditContract
{
    const TITLE = 'title';
    const DESCRIPTION = 'description';
    const ARTICLE_LINK = 'article_link';
    const THUMBNAIL = 'thumbnail';
    const CATEGORY = 'categories';
    const PUBLISHED = 'published';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            self::TITLE => 'required|string',
            self::DESCRIPTION => 'required|string',
            self::ARTICLE_LINK => 'required|active_url',
            self::THUMBNAIL => 'image',
            self::PUBLISHED => 'boolean',
            self::CATEGORY => 'array|required',
            self::CATEGORY . '.*' => 'exists:categories,id'
        ];
    }

    public function hasPublishedAt()
    {
        return $this->has(self::PUBLISHED);
    }

    public function getTitle()
    {
        return $this->get(self::TITLE);
    }

    public function getDescription()
    {
        return $this->get(self::DESCRIPTION);
    }

    public function getArticleLink()
    {
        return $this->get(self::ARTICLE_LINK);
    }

    public function getThumbnail()
    {
        return $this->file(self::THUMBNAIL);
    }

    public function getCategories()
    {
        return $this->get(self::CATEGORY);
    }

    public function getPublishedAt()
    {
        return Carbon::now();
    }

    public function hasThumbnail()
    {
        return $this->hasFile(self::THUMBNAIL);
    }
}
