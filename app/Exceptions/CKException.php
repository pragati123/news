<?php
/**
 * Created by PhpStorm.
 * User: piyushkantm
 * Date: 25/06/16
 * Time: 17:32
 */

namespace App\Exceptions;


use Symfony\Component\HttpKernel\Exception\HttpException;

class CKException extends HttpException
{
    public static $statusCodes = [
        'not_found' => 404
    ];

    public static $errorCodes = [
        'thumbnail_not_found' => 001
    ];

    public static $messages = [
        'thumbnail_not_found' => "Requested Thumbnail couldn't be found."
    ];

    public function __construct($statusCode, $message, $errorCode)
    {
        parent::__construct($statusCode, $message, null, array(), $errorCode);
    }
}