<?php
/**
 * Created by PhpStorm.
 * User: piyushkantm
 * Date: 25/06/16
 * Time: 17:37
 */

namespace App\Exceptions;


class ThumbnailNotFoundException extends CKException
{
    protected $message;
    protected $errorCode;
    protected $statusCode;

    public function __construct()
    {
        $this->message = CKException::$messages['thumbnail_not_found'];
        $this->errorCode = CKException::$errorCodes['thumbnail_not_found'];
        $this->statusCode = CKException::$statusCodes['not_found'];

        parent::__construct($this->statusCode, $this->message, $this->errorCode);
    }
}