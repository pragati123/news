<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Services\CKGoogleService;
use App\Story;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UrlShortenerJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $story;

    /**
     * Create a new job instance.
     * @param Story $story
     */
    public function __construct(Story $story)
    {
        $this->story = $story;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $service = new CKGoogleService();
        $shortLink = $service->generateShortLink($this->story->article_link);

        $this->story->short_url = $shortLink;
        $this->story->save();
    }
}
